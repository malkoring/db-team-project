# sage : python3 generate_screening_table.py <mpnumber> <YY-MM-DD> <hh:mm> <audnumber> <num of days>

python3 generate_screening_table.py 2 2017-12-09 06:00 1 25
python3 generate_screening_table.py 5 2017-12-02 06:00 2 89
python3 generate_screening_table.py 10 2017-12-01 06:00 3 30
python3 generate_screening_table.py 17 2017-12-17 06:00 5 32
python3 generate_screening_table.py 7 2018-01-06 06:00 1 54
python3 generate_screening_table.py 26 2017-12-31 06:00 3 58
python3 generate_screening_table.py 21 2018-02-01 06:00 5 18



python3 generate_screening_table.py 3 2017-12-03 08:30 1 12
python3 generate_screening_table.py 1 2017-12-01 08:30 2 52
python3 generate_screening_table.py 14 2017-12-17 08:30 3 16
python3 generate_screening_table.py 25 2017-12-01 08:30 4 81
python3 generate_screening_table.py 13 2017-12-01 08:30 5 14
python3 generate_screening_table.py 10 2017-12-25 08:30 1 11
python3 generate_screening_table.py 15 2018-01-22 08:30 2 38
python3 generate_screening_table.py 15 2018-01-20 08:30 3 30
python3 generate_screening_table.py 17 2017-12-15 08:30 5 4
python3 generate_screening_table.py 16 2018-01-06 08:30 1 10
python3 generate_screening_table.py 17 2018-01-04 08:30 5 4




python3 generate_screening_table.py 20 2017-12-01 11:00 1 6
python3 generate_screening_table.py 11 2017-12-15 11:00 2 76
python3 generate_screening_table.py 28 2017-12-01 11:00 3 18
python3 generate_screening_table.py 29 2017-12-03 11:00 4 28
python3 generate_screening_table.py 26 2017-12-01 11:00 5 16
python3 generate_screening_table.py 12 2017-12-08 11:00 1 48
python3 generate_screening_table.py 12 2018-01-12 11:00 3 6
python3 generate_screening_table.py 16 2017-12-31 11:00 4 60
python3 generate_screening_table.py 11 2017-12-18 11:00 5 4
python3 generate_screening_table.py 18 2018-01-27 11:00 1 8
python3 generate_screening_table.py 4 2018-02-02 11:00 3 27
python3 generate_screening_table.py 3 2018-01-06 11:00 5 38


python3 generate_screening_table.py 4 2017-12-03 13:30 1 23
python3 generate_screening_table.py 9 2017-12-01 13:30 2 90
python3 generate_screening_table.py 2 2017-12-12 13:30 3 18
python3 generate_screening_table.py 5 2018-01-02 13:30 4 2
python3 generate_screening_table.py 14 2017-12-02 13:30 5 32
python3 generate_screening_table.py 18 2017-12-26 13:30 1 32
python3 generate_screening_table.py 15 2018-01-04 13:30 3 8
python3 generate_screening_table.py 21 2018-01-06 13:30 4 22
python3 generate_screening_table.py 18 2018-01-03 13:30 5 15
python3 generate_screening_table.py 28 2018-02-15 13:30 1 14
python3 generate_screening_table.py 27 2018-01-12 13:30 3 20
python3 generate_screening_table.py 24 2018-01-06 13:30 4 40


python3 generate_screening_table.py 22 2017-12-01 16:00 1 24
python3 generate_screening_table.py 3 2017-12-01 16:00 2 67
python3 generate_screening_table.py 15 2017-12-01 16:00 3 30
python3 generate_screening_table.py 22 2017-12-10 16:00 4 69
python3 generate_screening_table.py 21 2017-12-07 16:00 5 22
python3 generate_screening_table.py 14 2018-01-27 16:00 1 19
python3 generate_screening_table.py 27 2018-02-06 16:00 2 23
python3 generate_screening_table.py 29 2018-01-01 16:00 3 53
python3 generate_screening_table.py 17 2017-12-23 16:00 5 12
python3 generate_screening_table.py 28 2017-12-28 16:00 1 25
python3 generate_screening_table.py 24 2018-01-06 16:00 5 8



python3 generate_screening_table.py 6 2017-12-02 18:30 1 13
python3 generate_screening_table.py 7 2017-12-01 18:30 3 24
python3 generate_screening_table.py 24 2017-12-01 18:30 4 39
python3 generate_screening_table.py 12 2017-12-02 18:30 5 50
python3 generate_screening_table.py 2 2017-12-15 18:30 1 23
python3 generate_screening_table.py 21 2017-12-25 18:30 3 22
python3 generate_screening_table.py 19 2018-01-09 18:30 4 50
python3 generate_screening_table.py 29 2018-01-21 18:30 5 31
python3 generate_screening_table.py 8 2018-01-07 18:30 1 21
python3 generate_screening_table.py 26 2018-01-18 18:30 3 12



python3 generate_screening_table.py 25 2017-12-07 21:00 1 16
python3 generate_screening_table.py 17 2017-12-20 21:00 2 71
python3 generate_screening_table.py 11 2017-12-01 21:00 3 11
python3 generate_screening_table.py 8 2017-12-01 21:00 4 30
python3 generate_screening_table.py 19 2017-12-01 21:00 5 16
python3 generate_screening_table.py 26 2017-12-25 21:00 1 34
python3 generate_screening_table.py 1 2017-12-27 21:00 3 16
python3 generate_screening_table.py 20 2017-12-31 21:00 4 60
python3 generate_screening_table.py 21 2018-01-08 21:00 5 28

