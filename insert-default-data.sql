
-- INSERTING MOVIE

/*
 * INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
 * VALUES ('영화제목', '관람등급', '주연이름', '상영시간', 런타임(정수), '장르', '감독이름');
 */

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('킬빌', 'R18', '우마 서먼', 180, '액션', '쿠엔틴타란티노');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('꾼','R15', '현빈', 117, '액션', '장창원');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('저스티스 리그', 'R12', '벤 애플렉', 120, '액션', '잭 스나이더');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('해피 데스데이', 'R15', '제시카 로테', 96, '액션 공포', '크리스토퍼 랜던');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('토르:라그나로크', 'R12', '크리스 헴스워스', 130, '액션', '타이카 와이티티');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('범죄도시', 'R19', '마동석', 121, '액션', '강윤성');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('키드냅', 'R15', '할리 베리', 94, '액션', '루이스 프리에토');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('인셉션', 'R12', '레오나르도 디카프리오', 147, '액션', '크리스토퍼 놀란');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('매트릭스', 'R18', '키아누 리브스', 136, 'SF', '릴리 워쇼스키');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('토이스토리3', 'ALL', '톰 행크스', 102, '애니메이션', '리 언크리치');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('킬월-E빌', 'ALL', '벤 버트', 104, '애니메이션', '앤드류 스탠튼');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('주토피아', 'ALL', '지니퍼 굿윈', 108, '애니메이션', '바이론 하워드');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('세 얼간이', 'R12', '아미르 칸', 171, '코미디', '라지쿠마르히라니');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('트루먼 쇼', 'R15', '짐 캐리', 103, '코미디', '피터 위어');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('죠스', 'R12', '로이 샤이더', 124, '공포', '스티븐 스필버그');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('직쏘', 'R19', '토빈 벨', 92, '공포', '마이클 스피어리그');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('그것', 'R15', '빌 스카스가드', 135, '공포', '안드레스 무시에티');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('애나벨:인형의 주인 ', 'R15', '스테파니 시그만', 109, '공포', '데이비드F.샌드버그');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('터미네이터2', 'R15', '아놀드 슈왈제네거', 137, 'SF', '제임스 카메론');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('에이리언:커버넌트', 'R15', '마이클 패스벤더', 122, 'SF', '리들리 스콧');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('라이프', 'R15', '제이크 질렌할', 103, 'SF', '다니엘 에스피노사');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('어바웃 타임', 'R15', '도널 글리슨', 123, '멜로', '리차드 커티스');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('타이타닉', 'R15', '레오나르도 디카프리오', 195, '멜로', '제임스 카메론');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('범죄와의 전쟁', 'R19', '최민식', 133, '범죄', '윤종빈');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('아가씨', 'R19', '김민희', 144, '스릴러', '박찬욱');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('신세계', 'R19', '이정재', 134 , '범죄', '박훈정');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('타짜', 'R19', '조승우', 139, '범죄', '최동훈');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('킹스맨:시크릿 에이전트', 'R19', '콜린 퍼스', 128, '액션', '매튜 본');

INSERT INTO MOVIE (mtitle, mcar, mactor, mruntime, mgenre, mdirector)
VALUES  ('킹스맨:골든 서클', 'R19', '콜린 퍼스', 141, '액션', '매튜 본');



-- INSERTING CREW

/*
 * INSERT INTO EMPLOYEE (ename, ehuman)
 * VALUES ('직원이름', '사람구분');
 *
 */

INSERT INTO EMPLOYEE (ename, ehuman)
VALUES ('공지민', '직원');

INSERT INTO EMPLOYEE (ename, ehuman)
VALUES ('김혜영', '직원');

INSERT INTO EMPLOYEE (ename, ehuman)
VALUES ('임은민', '직원');

INSERT INTO EMPLOYEE (ename, ehuman)
VALUES ('황명상', '직원');

INSERT INTO EMPLOYEE (ename, ehuman)
VALUES ('POS-001', '단말기');

INSERT INTO EMPLOYEE (ename, ehuman)
VALUES ('POS-002', '단말기');

-- INSERTING CUSTOMER

/*
 *
 * i) 회원
 *
 * WITH C AS (
 *   INSERT INTO CUSTOMER 
 *   ('전화번호', 나이(정수), '이름', '아이디', '패스워드')
 *   VALUES (phonenumber, age, cname, cid, cpw )
 *   RETURNING cnumber
 * ) INSERT INTO MEMBER (cnumber, crank, cpoint)
 * VALUES ((SELECT (C.cnumber) FROM C), '멤버십', 포인트(정수)); 
 *
 * ii) 비회원
 *
 * WITH C AS (
 *   INSERT INTO CUSTOMER (phonenumber, age, cname, cid, cpw)
 *   VALUES ('전화번호', 나이(정수), '이름', '아이디', '패스워드')
 *   RETURNING cnumber
 * ) INSERT INTO NON_MEMBER (cnumber,expiredate)
 * VALUES ((SELECT (C.cnumber) FROM C), '유효기간');
 *
 */

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-0000-1010', 24, '김정걸', 'jack', '123456')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'VIP', 8080);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-1230-2222', 24, '이재열', 'm_01012302222', '1qaz2wsx3edc' )
  RETURNING cnumber
) INSERT INTO NON_MEMBER ( cnumber, expiredate )
VALUES ((SELECT (C.cnumber) FROM C), '2017-12-30');

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-1212-2121', 18, '이시우', 'lcw1212', '33333333')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'GOLD', 500);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-5532-1231', 35, '이석현', 'hyun1231', '1132333')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'SILVER', 240);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-7633-1143', 35, '박경진', 'jin1143', 'qwer1234')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'VIP', 6300);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-5633-3343', 25, '도찬호', 'ho3341', 'qqhd1237')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'GOLD', 3600);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-9983-3323', 17, '민정환', 'mjh9983', 'hwan1163')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'BRONZE', 80);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-1208-9954', 27, '임미연', 'mee9954', 'aldus1208')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'SILVER', 1200);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-2268-9394', 22, '진수연', 'm_53217643212', '6jkl3nuv2fdq' )
  RETURNING cnumber
) INSERT INTO NON_MEMBER ( cnumber, expiredate )
VALUES ((SELECT (C.cnumber) FROM C), '2017-12-15');

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-8532-1112', 28, '차중호', 'm_18547678542', '9qen3kei2wsa' )
  RETURNING cnumber
) INSERT INTO NON_MEMBER ( cnumber, expiredate )
VALUES ((SELECT (C.cnumber) FROM C), '2017-11-13');

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-2508-7785', 24, '김다솜', 'm_25489678541', '5qwu2xsd1qwe' )
  RETURNING cnumber
) INSERT INTO NON_MEMBER ( cnumber, expiredate )
VALUES ((SELECT (C.cnumber) FROM C), '2017-11-22');

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-2662-3221', 26, '김효진', 'm_77854468795', '8fsc2qwy7cfs' )
  RETURNING cnumber
) INSERT INTO NON_MEMBER ( cnumber, expiredate )
VALUES ((SELECT (C.cnumber) FROM C), '2017-12-05');

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-9987-2215', 18, '곽성훈', 'm_77854467845', '8fsc2qwu7rre' )
  RETURNING cnumber
) INSERT INTO NON_MEMBER ( cnumber, expiredate )
VALUES ((SELECT (C.cnumber) FROM C), '2017-12-12');

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-1123-4456', 31, '김지인', 'm_77854467995', '8fsc2qwu1qpo' )
  RETURNING cnumber
) INSERT INTO NON_MEMBER ( cnumber, expiredate )
VALUES ((SELECT (C.cnumber) FROM C), '2017-12-12');

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-8843-1123', 41, '윤기주', 'm_77985412563', '8fsc5wqs1ddw' )
  RETURNING cnumber
) INSERT INTO NON_MEMBER ( cnumber, expiredate )
VALUES ((SELECT (C.cnumber) FROM C), '2017-12-27');

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-2156-8546', 22, '오준혁', 'm_77985415874', '8fsc5wqs2qoi' )
  RETURNING cnumber
) INSERT INTO NON_MEMBER ( cnumber, expiredate )
VALUES ((SELECT (C.cnumber) FROM C), '2017-12-27');

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-6539-1187', 22, '김보영', 'm_77985558749', '8fsc6gbc1aqw' )
  RETURNING cnumber
) INSERT INTO NON_MEMBER ( cnumber, expiredate )
VALUES ((SELECT (C.cnumber) FROM C), '2017-12-28');

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-6221-6765', 28, '최현우', 'm_77981854989', '8fsc8mmj4poi' )
  RETURNING cnumber
) INSERT INTO NON_MEMBER ( cnumber, expiredate )
VALUES ((SELECT (C.cnumber) FROM C), '2017-12-30');

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-8875-1297', 27, '오은주', 'joo1297', 'dmswn1297')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'GOLD', 5600);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-4456-5852', 25, '우민성', 'minsung5852', 'alstjd6621')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'SILVER', 2700);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-1109-0876', 18, '김정걸', 'boram0876', 'qhfka1109')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'BRONZE', 700);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-7454-0542', 31, '손정훈', 'hooon0542', 'thswjdgns112')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'BRONZE', 1000);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-1221-7432', 29, '현예림', 'hyleem7432', 'dpfla7292')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'VIP', 10500);

WITH C AS (
  INSERT INTO CUSTOMER ( phonenumber, age, cname, cid, cpw )
  VALUES ('010-5052-7560', 22, '우민성', 'minsung7560', 'alstjd123e32')
  RETURNING cnumber
) INSERT INTO MEMBER (cnumber, crank, cpoint)
VALUES ((SELECT (C.cnumber) FROM C), 'GOLD', 8080);



-- INSERTING screening_table

/*
 *
 * INSERT INTO SCREENING_TABLE ( starttime, period, capacity, audnumber )
 * VALUES ( 22:00:00, 2018-11-01, 120, 1);
 *
 * INSERT INTO SCREENING_TABLE ( starttime, period, capacity, audnumber )
 * VALUES ( 08:00:00, 2018-11-03, 100, 2);
 *
 */
 
 
 
