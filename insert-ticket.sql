----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 21, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 30 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 30));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (23, (SELECT TMP.tnumber FROM TMP), 30);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 120 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 120));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 120);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 150 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 150));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 150);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 190 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 190));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 190);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 255 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 255));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 255);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 255 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 255));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 255);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 255 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 255));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 255);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 255 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 255));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 255);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 255 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 255));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 255);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 255 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 255));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 255);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 255 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 255));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 255);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 255 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 255));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 255);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 255 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 255));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 255);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 255 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 255));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 255);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 255 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 255));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 255);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 255 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 255));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 255);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 300 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 300));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 300);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 300 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 300));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 300);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 300 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 300));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 300);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 300 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 300));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 300);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 300 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 300));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 300);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 300 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 300));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 300);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 300 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 300));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 300);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 300 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 300));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 300);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 300 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 300));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 300);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 300 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 300));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 300);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 300 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 300));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 300);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 300 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 300));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 300);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 300 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 300));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 300);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 307 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 307));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (23, (SELECT TMP.tnumber FROM TMP), 307);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 322 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 322));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 322);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 360 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 360));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 360);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 450 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 450));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 450);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 450 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 450));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 450);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 450 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 450));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 450);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 450 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 450));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 450);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 450 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 450));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 450);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 450 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 450));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 450);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 450 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 450));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 450);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 450 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 450));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 450);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 450 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 450));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 450);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 450 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 450));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 450);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 520 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 520));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 520);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 550 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 550));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 550);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 550 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 550));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 550);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 550 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 550));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 550);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 550 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 550));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 550);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 550 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 550));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 550);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 550 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 550));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 550);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 550 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 550));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 550);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 550 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 550));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 550);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 550 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 550));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 550);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 550 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 550));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 550);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 550 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 550));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 550);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 550 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 550));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 550);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 550 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 550));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 550);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 570);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 580);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 600 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 600));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 600);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 680 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 680));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 680);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 21, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 22, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 23, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 710 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 710));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (23, (SELECT TMP.tnumber FROM TMP), 710);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 750 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 750));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 750);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 21, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 810 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 810));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 810);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 870 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 870));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 870);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 21, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 22, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 900 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 900));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 900);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 914 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 914));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (23, (SELECT TMP.tnumber FROM TMP), 914);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 935 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 935));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 935);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 970 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 970));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 970);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1010 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1010));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (23, (SELECT TMP.tnumber FROM TMP), 1010);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1050 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1050));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 1050);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1090 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1090));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1090);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1130 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1130));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 1130);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1130 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1130));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1130);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1130 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1130));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1130);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1130 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1130));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1130);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1130 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1130));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1130);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1130 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1130));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1130);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1130 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1130));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1130);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1130 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1130));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1130);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1130 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1130));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1130);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1130 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1130));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1130);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1130 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1130));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1130);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1130 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1130));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1130);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1130 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1130));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1130);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1170 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1170));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1170);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1210 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1210));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 1210);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 21, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 22, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 23, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1230 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1230));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (23, (SELECT TMP.tnumber FROM TMP), 1230);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1260 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1260));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 1260);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1330 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1330));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 1330);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1400 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1400));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1400);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 21, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 22, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1430 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1430));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (23, (SELECT TMP.tnumber FROM TMP), 1430);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1460 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1460));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 1460);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1460 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1460));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1460);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1460 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1460));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1460);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1460 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1460));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1460);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1460 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1460));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1460);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1460 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1460));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1460);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1460 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1460));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1460);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1460 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1460));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1460);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1460 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1460));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1460);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1460 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1460));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1460);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1490 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1490));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1490);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1490 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1490));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1490);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1490 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1490));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1490);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1490 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1490));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1490);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1490 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1490));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1490);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1490 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1490));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1490);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1490 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1490));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1490);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1490 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1490));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1490);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1490 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1490));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1490);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1490 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1490));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1490);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 21, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 22, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 23, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1540 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1540));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (23, (SELECT TMP.tnumber FROM TMP), 1540);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1570);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1570 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1570));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1570);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1580 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1580));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1580);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 21, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 22, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 23, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1582 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1582));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (23, (SELECT TMP.tnumber FROM TMP), 1582);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1622 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1622));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1622);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1662 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1662));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 1662);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1722 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1722));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1722);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1722 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1722));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1722);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1722 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1722));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1722);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1722 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1722));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1722);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1722 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1722));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1722);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1722 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1722));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1722);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1722 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1722));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1722);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1722 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1722));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1722);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1722 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1722));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1722);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1722 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1722));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1722);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1722 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1722));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1722);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1722 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1722));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1722);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1722 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1722));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1722);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1752 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1752));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1752);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1752 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1752));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1752);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1752 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1752));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1752);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1752 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1752));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1752);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1752 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1752));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1752);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1752 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1752));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1752);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1752 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1752));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1752);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1752 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1752));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1752);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1752 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1752));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1752);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1752 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1752));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1752);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1799 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1799));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1799);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1830 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1830));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (23, (SELECT TMP.tnumber FROM TMP), 1830);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 21, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 22, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 23, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1855 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1855));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (23, (SELECT TMP.tnumber FROM TMP), 1855);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 21, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 22, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 23, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1867 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1867));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (23, (SELECT TMP.tnumber FROM TMP), 1867);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1890 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1890));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1890);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1890 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1890));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1890);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1890 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1890));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1890);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1890 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1890));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1890);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1890 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1890));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1890);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1890 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1890));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1890);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1890 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1890));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1890);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1890 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1890));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1890);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1890 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1890));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1890);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1890 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1890));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 1890);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1960 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1960));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 1960);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 1990 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 1990));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 1990);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2029 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2029));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 2029);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2029 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2029));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 2029);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2029 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2029));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 2029);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2029 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2029));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 2029);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2029 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2029));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 2029);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2029 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2029));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 2029);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2029 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2029));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 2029);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2029 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2029));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 2029);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2029 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2029));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 2029);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2029 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2029));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 2029);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2029 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2029));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 2029);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2029 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2029));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 2029);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2070 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2070));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 2070);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2103 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2103));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 2103);

DROP TABLE TMP;


----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (5, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (6, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (7, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (8, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (9, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (10, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (11, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 12, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (12, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 13, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (13, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 14, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (14, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 15, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (15, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 16, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (16, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 17, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (17, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 18, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (18, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 19, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (19, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 20, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (4, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (20, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 21, 10000, 'NO DISCOUNT' , 'ONLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (21, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 22, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (22, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 23, 10000, 'NO DISCOUNT' , 'OFFLINE', 'ADULT') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 2147 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))

;INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), (SELECT P.mpnumber FROM PLAN P WHERE P.stnumber = 2147));

INSERT INTO RESERVATION (cnumber, tnumber, stnumber)
VALUES (23, (SELECT TMP.tnumber FROM TMP), 2147);

DROP TABLE TMP;


