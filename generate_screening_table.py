import sys
import random
import datetime

def generate_sql(mpnumber, period, starttime, audnumber, n):
    print("----")

    if audnumber == 1:
        capacity = "100"
    elif audnumber == 2:
        capacity = "150"
    elif audnumber == 3:
        capacity = "120"
    elif audnumber == 4:
        capacity = "80"
    else:
        capacity = "20"
        
    x=period
    x_as_datetime = datetime.datetime.strptime(str(period),"%Y-%m-%d")
    new_datetime = x_as_datetime + datetime.timedelta(days=n)
    period = new_datetime.strftime("%Y-%m-%d")
    
    sql_string = "CREATE TEMPORARY TABLE TMP AS ( "
    sql_string += "WITH T AS ( INSERT INTO SCREENING_TABLE ( starttime, period, capacity, audnumber) VALUES ( "
    sql_string += "'" + starttime + "'" + ", " + "'" + period + "'" + ", " + capacity + ", "  + audnumber +  " ) RETURNING stnumber ) SELECT T.stnumber FROM T );\n\n"

    sql_string += "INSERT INTO PLAN (mpnumber, stnumber)\n"
    sql_string += "VALUES (" + mpnumber \
                  + ", (SELECT TMP.stnumber FROM TMP));\n\n"
    
    sql_string += "DROP TABLE TMP;\n\n"
    
    print(sql_string)


if len(sys.argv) < 6:
    sys.stderr.write("Usage : python3 generate_ticket.py <mpnumber> <YY-MM-DD> <hh:mm> <audnumber> <num of days>\n")
else:
    print("----")
    for i in range(0, int(sys.argv[5])):
        generate_sql(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], i)
